       IDENTIFICATION DIVISION.
       PROGRAM-ID. Finalu.
       AUTHOR. "SAKSARAK"

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT INPUT1-FILE ASSIGN TO "trader4.dat"
           ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION. 
       FD  INPUT1-FILE.
       01  INPUT1-BUFFER.
           88 END-OF-INPUT1-FILE      VALUE HIGH-VALUES.
       05  MAIN-PID          PIC   9(2).
           05   MAIN-TID     PIC   9(4).
           05   MAIN-INCOME  PIC   9(6). 
       WORKING-STORAGE SECTION. 
       01  MAXPRO         pic    9(2) VALUE ZEROS .
       01  TOTAL-INCOME   PIC    9(10) VALUE ZEROS .

       01  PID-PROCESS    PIC    9(2) .
       01  PID-TOTAL      PIC    9(4).
       01  INCOME-PROCESS PIC    9(10).
   
       01  RPT-HEADER.
           05 FILLER   PIC X(10) VALUE "  PROVINCE".
           05 FILLER   PIC X(12) VALUE "    P INCOME".
           05 FILLER   PIC X(12) VALUE "      MEMBER".
           05 FILLER   PIC X(19) VALUE "      MEMBER INCOME".
       01  RPT-ROW.
           05    FILLER              PIC X(5) VALUE SPACES .
           05    RPT-MAIN-PID        PIC 9(2).
           05    FILLER              PIC X(6) VALUE SPACES .
           05    RPT-P-INCOME        PIC 9(10).
           05    FILLER              PIC X(6) VALUE SPACES .
           05    RPT-MEMBER          PIC 9(4) .
           05    FILLER              PIC X(10) VALUE SPACES .
           05    PRT-MEMBER-INCOME   PIC 9(6).
       01  RPT-FOOTER.
           05 FILLER  PIC X(19) VALUE "     MAX PROVINCE: ".
           05 RPT-MAXPRO  PIC  9(2).
           05 FILLER  PIC X(16) VALUE "    SUM INCOME: ".
           05 RPT-SUM-IN  PIC  9(10).
       
       PROCEDURE DIVISION .
       BEGIN.
           OPEN  INPUT  INPUT1-FILE 
           DISPLAY RPT-HEADER 
           PERFORM READ-LINE
           PERFORM PROCEDURE-COL  UNTIL END-OF-INPUT1-FILE 
           DISPLAY RPT-FOOTER 
           CLOSE INPUT1-FILE 
           GOBACK
           .
       

       PROCEDURE-COL.
           MOVE MAIN-PID TO PID-PROCESS
           MOVE ZEROS TO PID-TOTAL 
           PERFORM PROCEDURE-LINE UNTIL MAIN-PID NOT = PID-PROCESS 
           MOVE PID-PROCESS TO RPT-MAIN-PID
           

           DISPLAY RPT-ROW 
           .

       PROCEDURE-LINE.
           ADD MAIN-INCOME TO TOTAL-INCOME
           PERFORM READ-LINE           
           .

      
       READ-LINE.
           READ INPUT1-FILE 
           AT END
              SET END-OF-INPUT1-FILE TO TRUE 
           END-READ
           .
